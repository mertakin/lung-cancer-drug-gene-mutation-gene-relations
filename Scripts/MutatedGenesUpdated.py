"""
Updated to reduce time complexity

Get mutated genes from Cosmic data using all gene names

"""


fin = open('Cosmic.csv')
file1 = open("allgenes.txt","r")
file_output = open("mutatedgenes.txt","w")

lines = file1.readlines()
a = {}
for line in lines:
    key = line.split("\n")[0]
    a[key] = 1

for line in fin.readlines():
    id=line.split(',')[0]
    if id in a:
       file_output.write(line)
