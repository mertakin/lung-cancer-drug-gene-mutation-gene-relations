
"""
Get the all gene names  from filtered uniprot data

"""


file1 = open("uniprotdata.txt","r")
file_output = open("allgenes.txt","w")

lines = file1.readlines()
genes = []


for line in lines:
	gene = line.split("	")[-1].split(" ")[0]
	if gene not in genes:
		genes.append(gene)


genes = genes[1:]
for gene in genes:
	if "\n" in gene:
		file_output.write(gene)
	else:
		file_output.write(gene+"\n")
	

