# Lung Cancer Drug-Gene Mutation-Gene Relations

### Summary

This is bioinformatics project with the goal of comparing drug-gene mutation gene relations in lung cancer disease.

### Motivation

Lung cancer is the leading cause of cancer deaths in world-wide. It challenges working with complex data and suitably qualified. 

### Methodology

Find all genes related to Lung cancer from [Uniprot](https://www.uniprot.org/)

Find all drugs used for Lung cancer from [Drugbank](https://www.drugbank.ca/)

Find all mutations related to Lung cancer from [Cosmic](https://cancer.sanger.ac.uk/cosmic)

Compare drug-gen and mutation-gene relations in the context

### Getting Data

##### Get filltered data from Uniprot

```bash
sudo wget -O filename.txt 'http://www.uniprot.org/uniprot/?sort=score&desc=&compress=no&query=annotation:(type:disease%20%22lung%20cancer%22)%20AND%20organism:%22Homo%20sapiens%20(Human)%20[9606]%22&fil=&limit=10&force=noe&format=txt'
```
##### Get all data from Drugbank

```bash
curl -Lfv -o drugbank.zip -u username:password https://www.drugbank.ca/releases/5-0-10/downloads/all-full-database
```

##### Get all data from Cosmic

```bash
sftp 'username'@sftp-cancer.sanger.ac.uk
sftp> get /files/grch37/cosmic/v83/CosmicMutantExport.tsv.gz
```

##### Get filtered data from Cosmic with [Cancer Browser](http://cancer.sanger.ac.uk/cosmic/browse/tissue?hn=carcinoma&in=t&sn=lung&ss=all) manually

- Tissue selection: Lung

- Sub-tissue selection: Include All

- Histology selection: Carcinoma















